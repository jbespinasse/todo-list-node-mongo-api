const fs = require('fs-extra');
const path = require('path');
const childProcess = require('child_process');
const obfuscationJavascript = require('javascript-obfuscator');

const findInDir = (dir, filter, fileList = []) => {
  const files = fs.readdirSync(dir);

  files.forEach((file) => {
    const filePath = path.join(dir, file);
    const fileStat = fs.lstatSync(filePath);

    if (fileStat.isDirectory()) {
      findInDir(filePath, filter, fileList);
    } else if (filter.test(filePath)) {
      fileList.push(filePath);
    }
  });

  return fileList;
}

try {
    // Remove current build
    fs.removeSync('./dist/');
    // Copy front-end files
    fs.copySync('./src/public', './dist/public');
    fs.copySync('./src/views', './dist/views');

    fs.copySync('./src/data-api/role.json', './dist/data-api/role.json');
    fs.copySync('./src/data-api/status.json', './dist/data-api/status.json');
    fs.copySync('./src/data-api/access-role.json', './dist/data-api/access-role.json');
    fs.copySync('./src/data-api/mail-role.json', './dist/data-api/mail-role.json');
    fs.copySync('./src/data-api/multer.config.ts', './dist/data-api/multer.config.ts');

    fs.copySync('./src/mail-api/cocontractor/order-form.ejs', './dist/mail-api/cocontractor/order-form.ejs');
    fs.copySync('./src/mail-api/cocontractor/status.ejs', './dist/mail-api/cocontractor/status.ejs');
    fs.copySync('./src/mail-api/cocontractor/incident.ejs', './dist/mail-api/cocontractor/incident.ejs');
    fs.copySync('./src/mail-api/cocontractor/observation.ejs', './dist/mail-api/cocontractor/observation.ejs');

    fs.copySync('./src/mail-api/comanager/order-form.ejs', './dist/mail-api/comanager/order-form.ejs');
    fs.copySync('./src/mail-api/comanager/status.ejs', './dist/mail-api/comanager/status.ejs');
    fs.copySync('./src/mail-api/comanager/incident.ejs', './dist/mail-api/comanager/incident.ejs');
    fs.copySync('./src/mail-api/comanager/observation.ejs', './dist/mail-api/comanager/observation.ejs');

    fs.copySync('./src/mail-api/superadmin/order-form.ejs', './dist/mail-api/superadmin/order-form.ejs');

    fs.mkdirSync('./dist/daos');
    fs.mkdirSync('./dist/daos/User');
    fs.mkdirSync('./dist/entities');
    fs.mkdirSync('./dist/models');
    fs.mkdirSync('./dist/mongo');
    fs.mkdirSync('./dist/routes');
    fs.mkdirSync('./dist/routes/stats');
    fs.mkdirSync('./dist/shared');
    fs.mkdirSync('./dist/uploads');
    
    // Transpile the typescript files
    childProcess.exec('tsc --build tsconfig.prod.json', {}, (err, stdOut, stdErr) => {
        const files = findInDir('./build/', /\.js$/);

        for (file of files) {
            let contentFile = fs.readFileSync(file);
            let obfuscationFile = obfuscationJavascript.obfuscate(contentFile);

            fs.writeFileSync(file.replace('build/', 'dist/'), obfuscationFile.getObfuscatedCode());
            console.log(file.replace('build/', 'dist/') + ' saved.');
        }
    });
} catch (err) {
    console.log(err);
}
