import './LoadEnv'; // Must be the first import
import app from '@server';
import { logger } from '@shared';
const http = require('http');
const server = http.Server(app);

/** ---------------------------------
 * Socket
 * ---------------------------------- */
const io = require('socket.io')(server);
import { onSocketConnection, SocketEventControl } from '@shared';

io.on('connection', (socket: any) => onSocketConnection(socket));

const ioRouter = require('socket.io-events')();
ioRouter.on('*', SocketEventControl);
io.use(ioRouter);

// Start the server
const port = Number(process.env.PORT || 3000);
server.listen(port, () => logger.info('Express server started on port: ' + port));