import { Todo } from '../mongo/todo';
import { TodoModel } from '../models/todo';

export interface TodoModelDao {
    getOne: (email: string) => Promise<TodoModel | null>;
    getAll: (query?: any, populate?: Array<{collection: string, fields: string}>, select?: any) => Promise<TodoModel[]>;
    getAggregate: (aggregate: any) => Promise<any[]>;
    add: (todo: TodoModel) => Promise<any>;
    update: (query: any, todo: TodoModel) => Promise<void>;
    delete: (email: string) => Promise<void>;
}

export class TodoDao implements TodoModelDao {

    /**
     * @param email
     */
    public async getOne(query: any): Promise<TodoModel | null> {
        return Todo.findOne(query).exec();
    }

    /**
     *
     */
    public async getAll(query: any = {}, 
        populate?: Array<{collection: string, fields: string}>,
        select?: any): Promise<TodoModel[]> {

        const todos = Todo.find(query).sort({dateAdd: -1});

        if (select) {
            todos.select(select);
        }

        if (populate && populate.length) {
            for (const p of populate) {
                todos.populate(p.collection, p.fields || null);
            }
        }
        
        return todos.exec();
    }

    /**
     * Aggregate query 
     * @param aggregate 
     */
    public async getAggregate(aggregate: any): Promise<any[]> {
        return Todo.aggregate(aggregate).exec();
    } 

    /**
     * @param email
     */
    public async getById(id: string): Promise<TodoModel | null> {
        return Todo.findOne({_id: id}).exec();
    }

    /**
     *
     * @param todo
     */
    public async add(todo: TodoModel): Promise<any> {
        const todo2Add = new Todo(todo);
        return todo2Add.save();
    }

    /**
     *
     * @param todo
     */
    public async update(query: any, todo: TodoModel): Promise<void> {
        return Todo.updateOne(query, todo);
    }

    /**
     *
     * @param any
     */
    public async delete(query: any): Promise<void> {
        return Todo.deleteOne(query);
    }
}
