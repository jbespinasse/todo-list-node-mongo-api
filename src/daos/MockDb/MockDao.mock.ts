import jsonfile from 'jsonfile';
import path from 'path';

export class MockDaoMock {

    private readonly dbFilePath = path.resolve(__dirname, 'MockDb.json');


    protected openDb(): Promise<any> {
        return jsonfile.readFile(this.dbFilePath);
    }


    protected saveDb(db: any): Promise<any> {
        return jsonfile.writeFile(this.dbFilePath, db);
    }
}
