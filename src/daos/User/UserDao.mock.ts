import { MockDaoMock } from '../MockDb/MockDao.mock';
import { UserModelDao } from './UserDao';
import { UserModel } from '../../models/user';

export class UserDao extends MockDaoMock implements UserModelDao {

    constructor() {
        super();
    }

    public async getOne(email: string): Promise<UserModel | null> {
        try {
            const db = await super.openDb();
            for (const user of db.users) {
                if (user.email === email) {
                    return user;
                }
            }
            return null;
        } catch (err) {
            throw err;
        }
    }

    
    public async getAggregate(aggregate: any): Promise<any[]> {
        return Promise.resolve([]);
    }

    public async add(user: UserModel): Promise<void> {
        try {
            const db = await super.openDb();
            // user.id = getRandomInt();
            db.users.push(user);
            await super.saveDb(db);
        } catch (err) {
            throw err;
        }
    }

    public async getAll(): Promise<UserModel[]> {
        try {
            const db = await super.openDb();
            return db.users;
        } catch (err) {
            throw err;
        }
    }

    public async update(query: any, user: UserModel): Promise<void> {
        try {
            const db = await super.openDb();
            for (let i = 0; i < db.users.length; i++) {
                if (db.users[i].id === user.id) {
                    db.users[i] = user;
                    await super.saveDb(db);
                    return;
                }
            }
            throw new Error('User not found');
        } catch (err) {
            throw err;
        }
    }

    public async delete(id: string): Promise<void> {
        try {
            const db = await super.openDb();
            for (let i = 0; i < db.users.length; i++) {
                if (db.users[i].id === id) {
                    db.users.splice(i, 1);
                    await super.saveDb(db);
                    return;
                }
            }
            throw new Error('User not found');
        } catch (err) {
            throw err;
        }
    }
}
