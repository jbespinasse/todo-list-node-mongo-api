import { User } from '../../mongo/user';
import { UserModel } from '../../models/user';

export interface UserModelDao {
    getOne: (email: string) => Promise<UserModel | null>;
    getAll: (query?: any, populate?: Array<{collection: string, fields: string}>, select?: any) => Promise<UserModel[]>;
    getAggregate: (aggregate: any) => Promise<any[]>;
    add: (user: UserModel) => Promise<any>;
    update: (query: any, user: UserModel) => Promise<void>;
    delete: (email: string) => Promise<void>;
}

export class UserDao implements UserModelDao {

    /**
     * @param email
     */
    public async getOne(query: any): Promise<UserModel | null> {
        return User.findOne(query).exec();
    }

    /**
     *
     */
    public async getAll(query: any = {}, 
        populate?: Array<{collection: string, fields: string}>,
        select?: any): Promise<UserModel[]> {

        const users = User.find(query);

        if (!select) {
            users.select({ password: 0, access_token: 0, dateUpd: 0, salt: 0 });
        } else {
            users.select(select);
        }

        if (populate && populate.length) {
            for (const p of populate) {
                users.populate(p.collection, p.fields || null);
            }
        }
        
        return users.sort('role').exec();
    }

    /**
     * Aggregate query 
     * @param aggregate 
     */
    public async getAggregate(aggregate: any): Promise<any[]> {
        return User.aggregate(aggregate).exec();
    } 

    /**
     * @param email
     */
    public async getByEmail(email: string): Promise<UserModel | null> {
        return User.findOne({email: email}).exec();
    }

    /**
     *
     * @param user
     */
    public async add(user: UserModel): Promise<any> {
        const user2Add = new User(user);
        return user2Add.save();
    }

    /**
     *
     * @param user
     */
    public async update(query: any, user: UserModel): Promise<void> {
        return User.updateOne(query, user);
    }

    /**
     *
     * @param any
     */
    public async delete(query: any): Promise<void> {
        return User.deleteOne(query);
    }
}
