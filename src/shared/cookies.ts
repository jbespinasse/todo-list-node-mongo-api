import { CookieOptions } from 'express';

export const jwtCookieProps = Object.freeze({
    key: '_api_cookie_key',
    options: <CookieOptions>{
        path: (process.env.JWT_COOKIE_PATH),
        httpOnly: (process.env.HTTP_ONLY_COOKIE === 'true'),
        signed: (process.env.SIGNED_COOKIE === 'true'),
        maxAge: Number(process.env.COOKIE_JWT_EXP),
        domain: (process.env.COOKIE_DOMAIN),
        secure: (process.env.SECURE_COOKIE === 'true'),
        sameSite: (process.env.SAMESITE_COOKIE)
    },
});

export const jwtCookieRefresh = Object.freeze({
    keyRefresh: '_api_cookie_refresh',
    optionsRefresh: <CookieOptions>{
        path: (process.env.JWT_COOKIE_PATH),
        httpOnly: (process.env.HTTP_ONLY_COOKIE === 'true'),
        signed: (process.env.SIGNED_COOKIE === 'true'),
        maxAge: Number(process.env.COOKIE_JWT_EXP),
        domain: (process.env.COOKIE_DOMAIN),
        secure: (process.env.SECURE_COOKIE === 'true'),
        sameSite: (process.env.SAMESITE_COOKIE)
    },
});

export const jwtCookieUser = Object.freeze({
    keyUser: 'user',
    optionsUser: <CookieOptions>{
        path: (process.env.JWT_COOKIE_PATH),
        httpOnly: (process.env.HTTP_ONLY_COOKIE === 'true'),
        signed: (process.env.SIGNED_COOKIE === 'true'),
        maxAge: Number(process.env.COOKIE_JWT_EXP),
        domain: (process.env.COOKIE_DOMAIN),
        secure: (process.env.SECURE_COOKIE === 'true'),
        sameSite: (process.env.SAMESITE_COOKIE)
    },
});
