import { NextFunction, Request, Response } from 'express';
import { UNAUTHORIZED } from 'http-status-codes';
import { logger } from './Logger';
const globalConfig = require(`../${process.env.GLOBAL_CONFIG}`);

export const CorsControl = async (req: Request, res: Response, next: NextFunction) => {
  
  // Same origin donc le cors n'a plus raison d'être
  next();
  /* try {
    const whitelist = globalConfig.urlFront;
    const ip = req.get('origin');

    if (whitelist.indexOf(ip) !== -1) {
      next();
    } else {
      logger.error('Error server.');
      return res.status(UNAUTHORIZED).json();
    }
  
  } catch (err) {
      logger.error(err);
      return res.status(UNAUTHORIZED).json({
          error: err.message,
      });
  } */
}