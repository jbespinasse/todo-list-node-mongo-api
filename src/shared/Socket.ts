import { JwtService } from './JwtService';
import { paramMissingError } from './Misc';
const globalConfig = require(`../${process.env.GLOBAL_CONFIG}`);
const whitelist = globalConfig.urlFront;

const jwtService = new JwtService();

export const SocketEventControl = async (socket: any, args: any, next: any) => {

  let tokenDecoded: any;
  
  const token = socket.sock.handshake.query.token || null;

  if (!token) {
    return next(new Error(paramMissingError));
  }

  const origin = socket.sock.handshake.headers.origin || null;
  if (!origin || whitelist.indexOf(origin) === -1) {
    return next(new Error(paramMissingError));
  }

  try {
    tokenDecoded = await jwtService.decodeJwt(token);
  } catch (err) {
    return next(new Error(err));
  }

  if (tokenDecoded.exp < Date.now()) {
    return next();
  } else {
    return next(new Error('Authentication error'));
  }
}

export const onSocketConnection = (socket: any) => {

  socket.on('error', (data: any) => {
    // console.log('<{error}>', data);
    socket.emit('refreshToken', data);
  });

  socket.on('editingRow', (data: any) => {
    // console.log('<{editingRow}>', data);
    socket.broadcast.emit('rowIsEditing', data);
  });

  socket.on('editingRowStopped', (data: any) => {
    // console.log('<{editingRowStoped}>', data);
    socket.broadcast.emit('rowIsStopped', data);
  });
}