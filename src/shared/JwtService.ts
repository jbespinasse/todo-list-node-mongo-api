import randomString from 'randomstring';
import jsonwebtoken, { VerifyErrors } from 'jsonwebtoken';

export interface IClientData {
    email?: string;
    id?: string;
    firstname?: string;
    lastname?: string;
    role?: string;
}

export interface IRefreshToken {
    id?: string;
    salt?: string;
    exp?: number;
}

export class JwtService {

    private readonly secret: string;
    private readonly options: object;
    private readonly VALIDATION_ERROR = 'JSON-web-token validation failed.';


    constructor() {
        this.secret = (process.env.JWT_SECRET || randomString.generate(100));
        this.options = {expiresIn: process.env.COOKIE_JWT_EXP};
    }


    /**
     * Encrypt data and return jwt.
     *
     * @param data
     */
    public getJwt(data: IClientData | IRefreshToken, secret?: string | null, options?: any): Promise<string> {
        return new Promise((resolve, reject) => {
            jsonwebtoken.sign(data, secret || this.secret, options || this.options, (err: any, token: any) => {
                err ? reject(err) : resolve(token);
            });
        });
    }


    /**
     * Decrypt JWT and extract client data.
     *
     * @param jwt
     */
    public decodeJwt(jwt: string, secret?: string): Promise<IClientData | IRefreshToken | any> {
        return new Promise((res, rej) => {
            jsonwebtoken.verify(jwt, secret || this.secret, (err, decoded) => {
                return err ? rej(this.VALIDATION_ERROR) : res(decoded);
            });
        });
    }
}
