import { logger } from './Logger';

const tedious = require('tedious');
const globalConfig = require(`../../../${process.env.GLOBAL_CONFIG}`);

export class Mssql {

  private _connection: any;

  constructor() {
  }

  private _connect(): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      this._connection = new tedious.Connection(globalConfig.mssql);

      this._connection.on('connect', (err: any) => {
        if (err) {
          logger.error(err);
          return reject(err);
        } else {
          return resolve(true);
        }
      });
    })
  }

  private _close() {
    if (this._connection) {
      this._connection.close();
    }
  }

  private _execute(r: string) {
    return new Promise((resolve: any, reject: any) => {
      const request = new tedious.Request(r, (err: any) => {
        if (err) {
          logger.error(err);
          return reject(err);
        }
      });
  
      request.on('row', (columns: any) => {
        return resolve(columns);
      });
    });
  }

  async request(request: string) {
    const c = await this._connect();
    let result: any = [];

    if (c === true) {
      result = await this._execute(request);
    }

    this._close();
    return result;
  }
}