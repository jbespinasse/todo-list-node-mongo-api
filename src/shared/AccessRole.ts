import { NextFunction, Request, Response } from 'express';
import { UNAUTHORIZED, OK } from 'http-status-codes';
import { JwtService } from './JwtService';
import { accessRestricted } from './Misc';

const globalConfig = require(`../${process.env.GLOBAL_CONFIG}`);
const accessRole = require('../data-api/access-role.json');
const jwtService = new JwtService();

/**
 * Permet de récupérer les conditions d'une route 
 * avant d'exécuter la requête qu'elle contient
 * (voir data-api/access-role.json)
 * 
 * @param access 
 * @param tokenDecoded 
 */
const getAccessParams = (access: any, tokenDecoded: any): any => {
  let extraParams: any = {};

  for (const param in access.extraParams) {
    if (tokenDecoded[access.extraParams[param]]) {
      extraParams[param] = tokenDecoded[access.extraParams[param]];
    } else {
      extraParams[param] = access.extraParams[param];
    }
  }

  return extraParams;
}

/**
 * 
 * Décline une clé en plusieurs si celle-ci à un point
 * Ex. : observation.role devient ["observation", "role"]
 * 
 * @param key 
 */
const getSubObjectKey = (key: string): any => {
  
  let newKey: string[] = [];
  if (key.indexOf('.') >= 0) {
    newKey = key.split('.');
  } else {
    newKey = [key];
  }

  return newKey;
}

/**
 * Supprime le dernier string/ d'une uri
 * @param uri 
 */
const removeLastItemUri = (uri: string): string => {
  var to = uri.lastIndexOf('/');
  to = to == -1 ? uri.length : to + 1;
  return uri.substring(0, to);
}

/**
 * Contrôle l'accès aux routes
 * 
 * @param req 
 * @param res 
 * @param next 
 */
export const AccessRoleControl = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.authorization;

    if (!token) {
      throw Error(accessRestricted);
    } else {
      const tokenDecoded: any = await jwtService.decodeJwt(token) || {};

      if (!tokenDecoded.role || globalConfig.roles.indexOf(tokenDecoded.role) === -1) {
        throw Error(accessRestricted);
      } else {
  
        let uri       = req.baseUrl + req.url;
        const params  = Object.keys(req.params).join('/');
        
        if (params && req.method === "GET") { 
          uri = `${req.baseUrl}${req.route.path}`; // `${removeLastItemUri(uri)}:${params}`; 
        }

        for (const access of accessRole) {
          if (access.role == tokenDecoded.role && access.uri == uri) {
            (req as any).extraParams = getAccessParams(access, tokenDecoded);
            (req as any).populateParams = access.populateParams || null;
            (req as any).selectParams = access.selectParams || null;
            (req as any).cleanResult = access.cleanResult || null;

            return next();
          }
        }
        
        if (tokenDecoded.role === globalConfig.superAdminRole) {
          return next();
        }

        /** return res.status(FORBIDDEN).json({ error: accessRestricted }); */

        return res.status(OK).json([]);
      }
    }
  } catch (err) {
    return res.status(UNAUTHORIZED).json({
        error: err.message,
    });
  }
}

/**
 * Modifie clé objet
 * 
 * @param obj 
 * @param key 
 * @param value 
 */
const modify = (obj: any, key: string, value: string): any => {
  let o = obj.toJSON();
  Object.keys(o).forEach((k) => { if ( k == key) delete obj[key]; });
  o[key] = value;
  return o;
}

/**
 * Remplacer une partie des résultats d'une requête par une autre valeur
 * (voir data-api/access-role.json)
 * 
 * @param results 
 * @param conditions conditions: {condition: {key: string; value: string; replaceAll?: boolean; }, newValue: string; }[]
 */
export const cleanResult = (
  results: any[], 
  conditions: {
    condition: { key: string; value: string; replaceAll?: boolean; }, 
    newValue: string; 
  }[]): any[] => {

  try {
    if (conditions && conditions && conditions.length
      && results && results.length) {
      for (let i=0; i<results.length; i++) {
        for (const c of conditions) {
    
          const key = getSubObjectKey(c.condition.key);
          if (key.length > 1 && results[i][key[0]] && results[i][key[0]][key[1]] && results[i][key[0]][key[1]] == c.condition.value) {
            if (c.condition.replaceAll) {
              results[i] = modify(results[i], key[0], c.newValue);
            } else {
              results[i].toJSON()[key[0]].toJSON()[key[1]] = c.newValue;
            }
          } else if (results[i][key[0]] && results[i][key[0]] == c.condition.value) {
            results[i] = modify(results[i], key[0], c.newValue);
          }
        }
      }
    }
  } catch (err) {
    throw err;
  }

  return results;
}