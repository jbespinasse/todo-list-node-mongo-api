import { NextFunction, Request, Response } from 'express';
import { UNAUTHORIZED } from 'http-status-codes';
import { JwtService } from './JwtService';

const jwtService = new JwtService();

export const TokenControl = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.authorization;
    if (!token) {
      throw Error('Mauvais token');
    } else {
      const tokenDecoded: any = await jwtService.decodeJwt(token) || {};
      // expire date
      if (tokenDecoded && tokenDecoded.exp < Date.now()) {
        next();
      } else {
        throw Error('Mauvais token');
      }
    }
  } catch (err) {
    return res.status(UNAUTHORIZED).json({
        error: err.message,
    });
  }
}