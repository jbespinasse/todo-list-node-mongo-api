import { Request, Response, Router } from 'express';
import { BAD_REQUEST, CREATED, OK } from 'http-status-codes';
import { UserDao } from '@daos';
import { UserModel } from '../models/user';

const bcrypt = require('bcrypt');
const globalConfig = require(`../${process.env.GLOBAL_CONFIG}`);

import {
    paramMissingError,
    AccessRoleControl,
    userExistsError,
    CorsControl,
    logger,
    TokenControl,
    JwtService,
    IClientData
} from '@shared';

// Init shared
const router = Router();
const userDao = new UserDao();

const hashPassword = (passwd: string): Promise<string> => {
    const salt = bcrypt.genSaltSync(globalConfig.bcrypt.salt);
    return bcrypt.hash(passwd, salt);
};

const randomString = (): string => {
    let ans     = ''; 
    const arr   = 'ABCDEFGHIJKLMOPQRSTUVWXYZ123456789';

    for (let i = 9; i > 0; i--) {
        ans +=  
            arr[Math.floor(Math.random() * arr.length)]; 
    } 
    return ans; 
};

/**
 * Générer un mot de passe en ligne
 * curl http://127.0.0.1:3000/api/users/passwd
 */
/* router.get('/passwd', async (req: Request, res: Response) => {

    const _randomString: string = randomString();
    const _hashPassword: string = await hashPassword(_randomString);

    console.log(_randomString, _hashPassword);
}); */

/************************************************************
 * DEBUT curl http://127.0.0.1:3000/api/users/create-first-user
************************************************************/
/* const getJwt = (user: UserModel): IClientData => {
    return {
        id: user._id,
        email: user.email,
        firstname: user.firstname,
        lastname: user.lastname,
        role: user.role
    };
};
const jwtService = new JwtService();
const saltRounds = 10;

router.get('/create-first-user', async (req: Request, res: Response) => {
    try {
        // Check parameters
        const user = <UserModel>{
            email: 'user@demo.com',
            firstname: 'User',
            lastname: 'Demo',
            role: 'superAdmin',
            password: 'test'
        };
        console.log(user)

        const userExists = await userDao.getOne({email: user.email});
        if (userExists && userExists._id) {
            return res.status(BAD_REQUEST).json({ message: userExistsError });
        }

        if (user.password) {
            user.password = await hashPassword(user.password);
        } else {
            user.password = await hashPassword(randomString());
        }

        // Add new user
        const newUser = await userDao.add(user);

        const jwt = await jwtService.getJwt(getJwt(user));
        const salt = bcrypt.genSaltSync(saltRounds);
        const jwt2 = await jwtService.getJwt(
            { id: user._id, salt: salt },
            null,
            { expiresIn: process.env.REFRESH_COOKIE_JWT_EXP }
        );

        newUser.salt = salt;
        await newUser.save();
        
        // Return
        return res.status(OK).json({
            token: jwt,
            refresh: jwt2,
            user: {
                email: user.email,
                firstname: user.firstname,
                lastname: user.lastname,
                role: user.role
            }
        });
        
    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({ message: err.message });
    }
}); */
/************************************************************
 * END curl http://127.0.0.1:3000/api/users/create-first-user
************************************************************/

/******************************************************************************
 *                      Get All Users - "GET /api/users/all"
 ******************************************************************************/

router.get('/all', [CorsControl, TokenControl, AccessRoleControl], async (req: Request, res: Response) => {
    try {
        const users = await userDao.getAll((req as any).extraParams || {});
        return res.status(OK).json(users);
    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({
            message: err.message,
        });
    }
});

router.get('/all/:role', [CorsControl, TokenControl, AccessRoleControl], async (req: Request, res: Response) => {
    try {
        const role = req.params.role || null;

        if (!role) {
            return res.status(BAD_REQUEST).json({
                message: paramMissingError,
            });
        }

        const users = await userDao.getAll(
            { ...((req as any).extraParams) || {}, ...{ role: role } } || { role: role }, 
            (req as any).populateParams,
            (req as any).selectParams
            )
        ;
        return res.status(OK).json(users);
    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({
            message: err.message,
        });
    }
});

router.get('/contributor', [CorsControl, TokenControl, AccessRoleControl], async (req: Request, res: Response) => {
    try {
        let companies = await userDao.getAggregate([
            {
                $group: {
                    _id: "$company",
                    "original": {
                        $first: "$$ROOT"
                    }
                }
            }, {
                $project: {
                    "company": "$_id",
                    "_id": "$original._id",
                    "role": "$original.role"
                }
            },
            {
                $match: { "role": { "$in": [ 'cocontractor', 'subcontractor' ] } }
            }
        ]);

        if ((req as any).extraParams && (req as any).extraParams._id) {
            companies = companies.filter((c: any) => c._id == (req as any).extraParams._id && c.company);
        } else {
            companies = companies.filter((c: any) => c.company);
        }

        return res.status(OK).json(companies);
    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({
            message: err.message,
        });
    }
});

/******************************************************************************
 *                       Add One - "POST /api/users/add"
 ******************************************************************************/

router.post('/add', [CorsControl, TokenControl, AccessRoleControl], async (req: Request, res: Response) => {
    try {
        // Check parameters
        const user = <UserModel>req.body;
        if (!user) {
            return res.status(BAD_REQUEST).json({ message: paramMissingError });
        }

        const userExists = await userDao.getOne({email: user.email});
        if (userExists && userExists._id) {
            return res.status(BAD_REQUEST).json({ message: userExistsError });
        }

        if (user.password) {
            user.password = await hashPassword(user.password);
        } else {
            user.password = await hashPassword(randomString());
        }

        // Add new user
        const newUser = await userDao.add(user);
        return res.status(CREATED).json({_id: newUser._id});
    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({ message: err.message });
    }
});

/******************************************************************************
 *                       Update - "PUT /api/users/update"
 ******************************************************************************/

router.put('/update', [CorsControl, TokenControl, AccessRoleControl], async (req: Request, res: Response) => {
    try {
        // Check Parameters
        const user = <UserModel>req.body;
        if (!user || !user._id) {
            return res.status(BAD_REQUEST).json({
                message: paramMissingError,
            });
        }

        const userExists = await userDao.getOne({email: user.email, _id: { $ne: user._id } });
        if (userExists && userExists._id) {
            return res.status(BAD_REQUEST).json({
                message: userExistsError,
            });
        }

        if (user.dateAdd) {
            delete user.dateAdd;
        }

        if (!user.authorized_connection || !user.active) {
            user.salt = '';
        }

        if (user.password) {
            user.password = await hashPassword(user.password);
        } else {
            delete user.password;
        }

        let query = (req as any).extraParams || { _id: user._id };
        // Update user
        await userDao.update(query, user);
        return res.status(OK).json({success: true});
    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({
            message: err.message,
        });
    }
});


/******************************************************************************
 *                    Delete - "DELETE /api/users/delete/:id"
 ******************************************************************************/

router.delete('/delete/:id', [CorsControl, TokenControl, AccessRoleControl], async (req: Request, res: Response) => {
    try {
        const id = req.params.id;

        if (!id) {
            return res.status(BAD_REQUEST).json({
                message: paramMissingError
            });
        }

        await userDao.delete({ _id: id });
        return res.status(OK).json({success: true});

    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({
            message: err.message,
        });
    }
});

router.get('/showpasswd/:id', [CorsControl, TokenControl, AccessRoleControl], async (req: Request, res: Response) => {
    try {
        const id = req.params.id;

        if (!id) {
            return res.status(BAD_REQUEST).json({
                message: paramMissingError
            });
        }
        
        const _randomString: string = randomString();
        const _hashPassword: string = await hashPassword(_randomString);

        await userDao.update({ _id: id }, { password: _hashPassword });
        return res.status(OK).json({ passwd: _randomString });

    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({
            message: err.message,
        });
    }
});


/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;
