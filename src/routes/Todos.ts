import { Request, Response, Router } from 'express';
import { OK } from 'http-status-codes';
import { TodoDao } from '@daos';

const router = Router();
const todoDao = new TodoDao();

router.get('/all', [], async (req: Request, res: Response) => {

  const todos = await todoDao.getAll((req as any).extraParams || {});
  return res.status(OK).json(todos);
});

router.post('/create', [], async (req: Request, res: Response) => {

  const { todo } = req.body;
  const newTodo = await todoDao.add(todo);
  return res.status(OK).json(newTodo);
})

router.post('/update', [], async (req: Request, res: Response) => {

  const { todo } = req.body;
  await todoDao.update({ _id: todo._id }, todo);
  return res.status(OK).json(todo);
})

router.post('/delete', [], async (req: Request, res: Response) => {

  const { id } = req.body;
  await todoDao.delete({ _id: id });
  return res.status(OK).json({ id: id });
})

router.post('/update/all', [], async (req: Request, res: Response) => {

  const { todos } = req.body;
  for (const todo of todos) {
    await todoDao.update({ _id: todo._id }, todo);
  }

  return res.status(OK).json(todos);
})

export default router;