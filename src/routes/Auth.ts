import bcrypt from 'bcrypt';
import { Request, Response, Router } from 'express';
import { BAD_REQUEST, OK, UNAUTHORIZED } from 'http-status-codes';
import { UserDao } from '@daos';

import {
    paramMissingError,
    loginFailedErr,
    refreshTokenExpired,
    userNotFound,
    logger,
    jwtCookieProps,
    JwtService,
    IClientData
} from '@shared';
import { UserModel } from 'src/models/user';

const router = Router();
const userDao = new UserDao();
const jwtService = new JwtService();
const saltRounds = 10;

const getJwt = (user: UserModel): IClientData => {
    return {
        id: user._id,
        email: user.email,
        firstname: user.firstname,
        lastname: user.lastname,
        role: user.role
    };
};

/******************************************************************************
 *                      Login User - "POST /api/auth/login"
 ******************************************************************************/

router.post('/login', async (req: Request, res: Response) => {
    try {
        // Check email and password present
        const { email, password } = req.body;

        if (!(email && password)) {
            return res.status(BAD_REQUEST).json({
                message: paramMissingError,
            });
        }

        // Fetch user
        const user = await userDao.getOne({email: email, authorized_connection: true, active: true});
        if (!user || !user.authorized_connection  || !user.active) {
            return res.status(UNAUTHORIZED).json({ message: loginFailedErr });
        }

        // Check password
        const pwdPassed = await bcrypt.compare(password, user.password);
        if (!pwdPassed) {
            return res.status(UNAUTHORIZED).json({ message: loginFailedErr });
        }

        // Setup token Cookie
        const jwt = await jwtService.getJwt(getJwt(user));

        // Setup refresh token Cookie
        var salt = bcrypt.genSaltSync(saltRounds);
        const jwt2 = await jwtService.getJwt({
            id: user._id,
            salt: salt
        }, null, { expiresIn: process.env.REFRESH_COOKIE_JWT_EXP });

        // On enregistre le salt généré,
        // On en aura besoin pour générer un nouveau token
        user.salt = salt;
        await user.save();

        // Return
        return res.status(OK).json({
            token: jwt,
            refresh: jwt2,
            user: {
                email: user.email,
                firstname: user.firstname,
                lastname: user.lastname,
                role: user.role
            }
        });

    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({
            message: err.message,
        });
    }
});

router.get('/isLogged', async (req: Request, res: Response) => {
    const token: string = req.headers.authorization || '';

    if (token) {
        try {
            const tokenDecoded: any | string = await jwtService.decodeJwt(token);
            if (tokenDecoded && tokenDecoded.exp < Date.now()) {
                return res.status(OK).json({ valid: true });
            }
        } catch(error) {
            // console.log(error);
        }
    }

    return res.status(OK).json({ valid: false });
});

router.post('/refresh', async (req: Request, res: Response) => {

    try {
        const refresh = req.body.refresh || null;
    
        if (refresh) {
            const refreshDecoded: any = await jwtService.decodeJwt(refresh) || {};
            if (refreshDecoded && refreshDecoded.exp < Date.now()) {
    
                // Fetch user
                const user = await userDao.getOne({ _id: refreshDecoded.id, salt: refreshDecoded.salt });
    
                // No user data
                if (!user) return res.status(UNAUTHORIZED).json({ message: userNotFound });
                
                // Setup Admin Cookie
                const jwt = await jwtService.getJwt(getJwt(user));
                return res.status(OK).json({ token: jwt });
    
            } else {
                return res.status(UNAUTHORIZED).json({ message: refreshTokenExpired });
            }
        }
        return res.status(UNAUTHORIZED).json({ message: refreshTokenExpired });
    } catch (err) {
        return res.status(UNAUTHORIZED).json({ message: refreshTokenExpired });
    }
});

/******************************************************************************
 *                      Logout - "GET /api/auth/logout"
 ******************************************************************************/

router.get('/logout', async (req: Request, res: Response) => {
    try {
        // const { key, options } = jwtCookieProps;
        // res.clearCookie('token', options);
        // res.clearCookie('refresh', options);
        // res.clearCookie('user', options);
        return res.status(OK).end();
    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({
            error: err.message,
        });
    }
});


/******************************************************************************
 *                                 Export Router
 ******************************************************************************/

export default router;
