const multer = require('multer');
const fs = require('fs');

var storage = multer.diskStorage({
  destination: (req: any, file: any, cb: any) => {

    const _path = `${__dirname}/../../uploads`;
    if (!fs.existsSync(_path)) {
      fs.mkdirSync(_path, { recursive: true });
      fs.chmod(_path, 511);
    }

    cb(null, _path);
  },
  filename: (req: any, file: any, cb: any) => {
    cb(null, file.originalname.replace(/\s/g, "-").toLowerCase())
  }
});
 
export const upload = multer({ storage: storage });
 