import cookieParser from 'cookie-parser';
import express from 'express';
import logger from 'morgan';
import path from 'path';
import BaseRouter from './routes';
const bodyParser = require('body-parser');

import { Request, Response } from 'express';
import { UNAUTHORIZED, INTERNAL_SERVER_ERROR } from 'http-status-codes';
const globalConfig = require(`./${process.env.GLOBAL_CONFIG}`);
const mongoose = require('mongoose');

const whitelist = globalConfig.urlFront;

// Mongoose
mongoose.connect(globalConfig.mongoUrl, globalConfig.mongoOptions);
 
const db = mongoose.connection; 
db.on('error', console.error.bind(console, 'Erreur lors de la connexion')); 
db.once('open', () => console.log("Connexion à la base OK"));

mongoose.set("debug", (collectionName: string, method: string, query: any, doc: any) => {
    console.log(`${collectionName}.${method}`, JSON.stringify(query), doc);
});

// Init express
const app = express();

const cors = require('cors');
// Add middleware/settings/routes to express.
app.use(logger('dev'));
app.use(cors({origin: true, credentials: true}));
app.use(cookieParser(process.env.COOKIE_SECRET));
app.use(bodyParser.json({ limit: '50mb' } ));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false } ));
app.use('/api', BaseRouter);

/** ---------------------------------
 * Serve front-end content.
 * ---------------------------------- */
const viewsDir = path.join(__dirname, 'views');
app.set('views', viewsDir);
const staticDir = path.join(__dirname, 'public');
app.use(express.static(staticDir));

app.use((err: any, req: Request, res: Response, next: any) => {
    console.error(err.stack);
    res.status(INTERNAL_SERVER_ERROR).send('Something broke!');
});

app.use((req: Request, res: Response, next: any) => {
    if (req.url == '/api/auth/login') {
        const token = req.headers.authorization || null;
        if (!token) {
            return res.status(UNAUTHORIZED).send('Unautorized!');
        }
    } else {
        next();
    }
});

app.use((req: Request, res: Response, next: any) => {
    const corsOptions = {
      origin: (origin: string, callback: any) => {
        if (whitelist.indexOf(origin) !== -1) {
            next();
        } else {
            new Error('Not allowed by Server');
        }
      }
    }
});

process.on('uncaughtException', (e) => console.log(e))
process.on('SIGTERM', (e) => console.log(e))

/* app.use(function(req, res, next) {
    res.status(NOT_FOUND).send('Not found');
}); */

// Export express instance
export default app;
