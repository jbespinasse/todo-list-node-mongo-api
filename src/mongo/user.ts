"use strict";

const globalConfig = require('../config.json');
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  email: { type: String, required: false, validate: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/ },
  password: { type: String, required: false },
  active: { type: Boolean, required: false, default: false },
  phone: { type: String, required: false },
  company: { type: String, required: false }, // { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: false },
  role: { type: String, in: globalConfig.roles },
  authorized_connection: { type: Boolean, required: false, default: false },
  access_token: { type: String, required: false },
  salt: { type: String, required: false },
  bgcolor: String,

  dateUpd: { type: Date, default: Date.now },
  dateAdd: { type: Date, default: Date.now }
});

export const User = mongoose.model('User', userSchema);