"use strict";

const mongoose = require('mongoose');

const todoSchema = new mongoose.Schema({
  text: { type: String, required: true },
  completed: { type: Boolean, required: false, default: false },

  dateUpd: { type: Date, default: Date.now },
  dateAdd: { type: Date, default: Date.now }
});

export const Todo = mongoose.model('Todo', todoSchema);