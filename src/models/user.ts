export interface UserModel {
  _id: string;
  firstname: string;
  lastname: string;
  email: string;
  password?: string;
  active: boolean;
  phone: string;
  company: any;
  role: "superAdmin" | "admin";
  authorized_connection: boolean;
  access_token: string;
  salt: string;
  dateAdd?: Date;
  dateUpd: Date;
  bgcolor?: string;
}