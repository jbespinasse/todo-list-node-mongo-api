export interface TodoModel {
  _id: string;
  text: string;
  completed: boolean;
  dateAdd?: Date;
  dateUpd: Date;
}